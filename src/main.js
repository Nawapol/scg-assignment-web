import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {routes} from './SCG'
import BootStrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import VeeValidate from 'vee-validate'
import vueHeadful from 'vue-headful'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import AOS from 'aos'
import Icon from 'vue-awesome/components/Icon'
import 'vue-awesome/icons'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import "bootstrap/js/dist/util"
import "bootstrap/js/dist/dropdown"
import 'swiper/dist/css/swiper.css'
import 'aos/dist/aos.css';

Vue.use(BootStrapVue)
Vue.use(VueRouter)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAsfoTulroFx4UDOd5mu632oZpKYaWxY7E',
    libraries: 'places,drawing,visualization' // necessary for places input
  }
});
Vue.use(VeeValidate, {
  fieldsBagName: 'veeFields'
})
Vue.use(VueAwesomeSwiper, /* { default global options } */)
Vue.component('vue-headful', vueHeadful);
Vue.component('v-icon', Icon)

const router = new VueRouter({
  routes,
  mode: 'history'
})

// Vue.config.productionTip = false

new Vue({
  el:'#app',
  router,
  created(){
    AOS.init();
  },
  render: h => h(App),
}).$mount('#app')
