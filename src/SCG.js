import Home from './views/Home.vue'
import FindSolution from './views/FindSolution.vue'
import PlaceAPI from './views/PlaceAPI.vue'
import MessagingAPI from './views/MessagingAPI.vue'

export const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/find-solution',
        component: FindSolution,
        name: 'find-solution'
    },
    {
        path: '/place-api',
        component: PlaceAPI,
        name: 'place-api'
    },
    {
        path: '/messaging-api',
        component: MessagingAPI,
        name: 'messaging-api'
    }
]